import React, {Component} from 'react';

class SelectListItem extends Component{
    render() {
        const { data, item, selectItem } = this.props;

        return <div className="b_select-list__item"
            onMouseDown={() => selectItem(item)}>
            <p dangerouslySetInnerHTML={{ __html: data }}></p>
        </div>;
    }
}

export default SelectListItem;