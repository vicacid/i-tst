import React, { Component } from 'react';
import SelectListItem from './select-list-item';

class SelectList extends Component {
    render() {
        const { list, filter, onSelectItem, position } = this.props;
        let filteredList = [];
        let options = [];

        function formatResult(string, filter) {
            const index = name.toLowerCase().indexOf(filter.toLowerCase());

            return filter.trim().length ?
                `<span class="filter-match">${string.substr(0, filter.length)}</span>${string.substr(filter.length, string.length)}` :
                string;
        }

        list.forEach(item => {
            const nameLower = item.name.toLowerCase();
            const filterLower = filter.toLowerCase();

            if (!nameLower.indexOf(filterLower)) {
                let result = nameLower.indexOf(filterLower);
                filteredList.push(
                    <SelectListItem key={item.name} item={item.name} data={formatResult(item.name, filter)} selectItem={onSelectItem} />
                );
            }

        });

        if (!filteredList.length) {
            filteredList.push(
                <SelectListItem key={-1} data={'No results found...'} />
            )
        }

        return <div className={'b_select-container__list b_select-list' + ' b_select-container__list_position_' + position}> {filteredList}</div>;
    }
}

export default SelectList;