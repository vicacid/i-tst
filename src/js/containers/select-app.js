import React, {Component} from 'react';
import { connect } from 'react-redux';
import { filterList, changeListVisibility, selectItem } from '../actions';
import SelectList from '../components/select-list';
import * as Utils from '../utils'

import './select-app.styl';

const countries = [
    { name: 'Австралия' },
    { name: 'Австрия' },
    { name: 'Азербайджан' },
    { name: 'Албания' },
    { name: 'Алжир' },
    { name: 'Американское Самоа' }
];

const isMobile = Utils.mobilecheck();

class SelectApp extends Component {
    render() {
        const { filter, isVisible, selectedItem, onFilter, onChangeVisibility, onSelectItem } = this.props;

        let input;
        let options = [];

        function getListPosition(itemsCount) {
            const documentHeight = document.documentElement.clientHeight;
            const elementBottomPosition = document.querySelector('.b_select-container__input').getBoundingClientRect().bottom;
            const listHeight = itemsCount * 60;
            const bottomSpaceEnough = documentHeight - elementBottomPosition > listHeight;
            const topSpaceEnough = documentHeight - (documentHeight - elementBottomPosition) > listHeight;

            return topSpaceEnough && !bottomSpaceEnough ? 'top' : 'bottom';
        }

        options.push(<option key={-1} value={''}>{''}</option>);
        countries.forEach(option => {
            options.push(<option key={option.name} value={option.name}>{option.name}</option>)
        });

        return (<div className="b_select-container">
                <div className={'b_select-container__input' + (isVisible ? ' b_select-container__input_list_visible' : '')}>
                    {!isMobile &&
                        <input type="text"
                            value={filter}
                            ref={node => { input = node; }}
                            onChange={() => onFilter(input.value)}
                            onBlur={() => onChangeVisibility(false)}
                            onFocus={() => onChangeVisibility(true)}
                        />
                    }

                    {isMobile &&
                        <select name="country"
                            onChange={(...args) => onSelectItem(args[0].nativeEvent.target.value)}
                            value={selectedItem}>{options}</select>
                    }

                    < span className="b_select-container__list-toggler">
                        <i className={'b_select-container__list-toggler_state' + (isVisible ? '_hidden' : '_visible')}></i>
                    </span>
                    <span className={'b_select-container__input-placeholder' +
                        (isVisible || (!isVisible && filter) ? ' active' : '')}>
                        Выберите страну
                    </span>
                </div>

                {!isMobile && isVisible && <SelectList filter={filter}
                    list={countries}
                    onSelectItem={onSelectItem}
                    position={getListPosition(countries.length)} />
                }
            </div >)
    }
}

const mapStateToProps = (state) => {
    return {
        filter: state.filter,
        isVisible: state.isVisible,
        selectedItem: state.selectedItem
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFilter: filterText => dispatch(filterList(filterText)),
        onChangeVisibility: isVisible => dispatch(changeListVisibility(isVisible)),
        onSelectItem: selectedItem => dispatch(selectItem(selectedItem)),
    };
};

//subscribing to the store
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectApp);