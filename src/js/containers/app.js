import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, compose, combineReducers } from 'redux';

import SelectApp from './select-app';
import * as reducers from '../reducers';

const reducer = combineReducers(reducers);
const store = createStore(reducer);

export default class App extends Component {
    render() {
        return (
            <div>
                <Provider store={store}>
                    <SelectApp />
                </Provider>
            </div>
        );
    }
}