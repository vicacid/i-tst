import 'babel-polyfill';
import '../styles/reset.css'
import React from 'react';
import { render } from 'react-dom';
import App from './containers/app';

render(<App />, document.getElementById('b_app'));