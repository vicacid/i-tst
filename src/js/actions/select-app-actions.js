import * as TYPES from './types';

export function filterList(filter) {
    return {
        type: TYPES.FILTER,
        filter
    };
}

export function changeListVisibility(isVisible) {
    return {
        type: TYPES.VISIBILITY,
        isVisible
    };
}

export function selectItem(selectedItem) {
    return {
        type: TYPES.SELECT_ITEM,
        selectedItem,
        filter: selectedItem
    };
}