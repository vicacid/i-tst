import * as TYPES from '../actions/types';

export const filter = (state = '', action) => {
    switch (action.type) {
        case TYPES.FILTER:
            return action.filter;
        case TYPES.SELECT_ITEM:
            return action.selectedItem;
        default:
            return state;
    }
};

export const isVisible = (state = false, action) => {
    switch (action.type) {
        case TYPES.VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}

export const selectedItem = (state = '', action) => {
    switch (action.type) {
        case TYPES.SELECT_ITEM:
            return action.selectedItem;
        default:
            return state;
    }
}
